Option Explicit

' This is the Sub that opens external files and reads in the contents.
' In this way, you can have separate files for data and libraries of functions
Sub Include(yourFile)

  Dim oFSO, oFileBeingReadIn   ' define Objects
  Dim sFileContents            ' define Strings
 
  Set oFSO = CreateObject("Scripting.FileSystemObject")
  Set oFileBeingReadIn = oFSO.OpenTextFile("C:\Users\icarvalho\Documents\VBA\Git\vbscripts\VBS Library.vbs", 1)
   
  sFileContents = oFileBeingReadIn.ReadAll
  oFileBeingReadIn.Close
  ExecuteGlobal sFileContents
  
End Sub

Include "VBS Library"

'================================================================================================================================

Dim sScriptdir
Dim iCountOfXLSMFiles
Dim sXLSMFileName

	sScriptdir = CreateObject("Scripting.FileSystemObject").GetParentFolderName(WScript.ScriptFullName)

	'---------------------------------------------------------------------------------------------------------------------------------
	'Count of XLSM files
	iCountOfXLSMFiles = GetContOfXLSMFiles_Lib(sScriptdir)

	If iCountOfXLSMFiles = 0 Then
		MsgBox "There is no XLSM file inside the sScriptdir Path."& Chr(13) & "It's necessary to have only 1 XLSM file.", vbCritical , "WARNING"
		WScript.Quit
	Elseif iCountOfXLSMFiles > 1 Then
		MsgBox "There are " & iCountOfXLSMFiles & " XLSM files inside the sScriptdir Path." & Chr(13) & "It's necessary to have only 1 XLSM file.", vbCritical, "WARNING"
		WScript.Quit	
	End if
	
	'---------------------------------------------------------------------------------------------------------------------------------
	'Get XLSM file name
	sXLSMFileName = GetXLSMFileName_Lib(sScriptdir)
	
	'---------------------------------------------------------------------------------------------------------------------------------
	'Export VBComponents
	Call ExportVbComponents_lib(sScriptdir, sXLSMFileName)

