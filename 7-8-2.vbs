Option Explicit

' This is the Sub that opens external files and reads in the contents.
' In this way, you can have separate files for data and libraries of functions
Sub Include(yourFile)

  Dim oFSO, oFileBeingReadIn   ' define Objects
  Dim sFileContents            ' define Strings
 
  Set oFSO = CreateObject("Scripting.FileSystemObject")
  Set oFileBeingReadIn = oFSO.OpenTextFile("C:\Users\icarvalho\Documents\VBA\Git\vbscripts\VBS Library.vbs", 1)
   
  sFileContents = oFileBeingReadIn.ReadAll
  oFileBeingReadIn.Close
  ExecuteGlobal sFileContents
  
End Sub

Include "VBS Library"

'================================================================================================================================

'---------------------------------------------------------------------------------------------------------------------------------
'Kill All Excel Instances
Call KillAllExcelInstances_Lib

Dim sScriptdir
Dim iCountOfXLSMFiles
Dim sXLSMFileName
Dim sCommit
Dim bolBashScriptPause
Dim sOriginPath
Dim sDestinationFolder

	sScriptdir = CreateObject("Scripting.FileSystemObject").GetParentFolderName(WScript.ScriptFullName)

	'---------------------------------------------------------------------------------------------------------------------------------
	'Count of XLSM files
	iCountOfXLSMFiles = GetContOfXLSMFiles_Lib(sScriptdir)

	If iCountOfXLSMFiles = 0 Then
		MsgBox "There is no XLSM file inside the sScriptdir Path."& Chr(13) & "It's necessary to have only 1 XLSM file.", vbCritical , "WARNING"
		WScript.Quit
	Elseif iCountOfXLSMFiles > 1 Then
		MsgBox "There are " & iCountOfXLSMFiles & " XLSM files inside the sScriptdir Path." & Chr(13) & "It's necessary to have only 1 XLSM file.", vbCritical, "WARNING"
		WScript.Quit	
	End if

	'---------------------------------------------------------------------------------------------------------------------------------
	'Get XLSM file name
	sXLSMFileName = GetXLSMFileName_Lib(sScriptdir)
	
	'---------------------------------------------------------------------------------------------------------------------------------
	'Copy Original Files From BRSCJOI
	sOriginPath = Replace(sScriptdir,"C:\Users\icarvalho\Documents\","\\brscjoi-fp01\Groups\SSC\SSC AUTOMATION\")
	sOriginPath = sOriginPath & "\Original Files\*.*"
	sDestinationFolder = sScriptdir & "\Original Files\"
	Call CopyFiles_Lib(sOriginPath, sDestinationFolder)
	
	'---------------------------------------------------------------------------------------------------------------------------------	
	'Copy XLSM Main File to BRSCJOI
	sOriginPath = sScriptdir & "\" & sXLSMFileName
	sDestinationFolder = Replace(sScriptdir,"C:\Users\icarvalho\Documents","\\brscjoi-fp01\Groups\SSC\SSC AUTOMATION") &"\"
	Call CopyFiles_Lib(sOriginPath, sDestinationFolder)

	'---------------------------------------------------------------------------------------------------------------------------------
	'Git Push (Default Commit)
	sCommit = "Update - " & FormatDateTime(Now, vbGeneralDate)
	bolBashScriptPause = False
	Call GitPush_Lib (sScriptdir, sCommit, bolBashScriptPause)

	'---------------------------------------------------------------------------------------------------------------------------------
	'Implementar rotina para copiar via VBS arquivos para BRSCJOI
	 MsgBox "Implementar rotina para copiar via VBS arquivos para BRSCJOI"

