Attribute VB_Name = "Mod__Format"
Option Explicit

Sub SpecificFormatSPL086000030(sExecutionID)

Dim sFunctionName                   As String
Dim sFunctionParameters             As String
Dim sExecutionExitMode              As String

sFunctionName = "Sub SpecificFormatSPL086000030"
sFunctionParameters = sExecutionID
Call PrintInFunction_Lib(sExecutionID, sUser, sFunctionName, sFunctionParameters)
    
    wsSPL086000030.Select
    wsSPL086000030.[A:C, E:E].HorizontalAlignment = xlCenter
    wsSPL086000030.[F:F].Style = "Comma"

sExecutionExitMode = "Traditional"
Call PrintOutFunction_Lib(sExecutionID, sUser, sExecutionExitMode, sFunctionName, sFunctionParameters)

End Sub

Sub SpecificFormatTemp(sExecutionID)

Dim sFunctionName                   As String
Dim sFunctionParameters             As String
Dim sExecutionExitMode              As String

sFunctionName = "Sub SpecificFormatTemp(sExecutionID)"
sFunctionParameters = sExecutionID
Call PrintInFunction_Lib(sExecutionID, sUser, sFunctionName, sFunctionParameters)

    wsTemp.Select
    wsTemp.[A:C,E:G,J:M,O:O,Q:Q].HorizontalAlignment = xlCenter

sExecutionExitMode = "Traditional"
Call PrintOutFunction_Lib(sExecutionID, sUser, sExecutionExitMode, sFunctionName, sFunctionParameters)

End Sub
