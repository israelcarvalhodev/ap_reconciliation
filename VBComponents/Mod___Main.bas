Attribute VB_Name = "Mod___Main"
Option Explicit

Sub Main()

Dim sExecutionID                        As String
Dim sReturn                             As String
Dim sFunctionName                       As String
Dim sFunctionParameters                 As String
Dim sExecutionExitMode                  As String

sExecutionID = Empty
sTestStatus = Empty
bolRunSuportFileScripts = False

sExecutionID = CreateGuidString_Lib()

sReturn = UiPath_RunFormatAPReconciliation(sExecutionID)

End Sub

Private Function UiPath_RunFormatAPReconciliation(sExecutionID As String) As String
'Fun��o que programa C# chamar�

Dim sAutomationName                     As String
Dim sFunctionName                       As String
Dim sFunctionParameters                 As String
Dim sExecutionExitMode                  As String

sExecutionID = UCase(sExecutionID)

sUser = GetUserName_Lib
sAutomationID = SetAutomationID
sAutomationName = GetAutomationName_Lib(sAutomationID, sUser)
sAutomationMainFileName = GetAutomationMainFileName_Lib(sAutomationID, sUser)

Call PrintExecutionID_Lib(sExecutionID, sUser, sAutomationID, sAutomationName, sAutomationMainFileName)

Call SetExecutionLog_Lib(sAutomationID, sExecutionID, sUser)

sFunctionName = "Private Function UiPath_RunFormatAPReconciliation(sExecutionID As String) As String"
sFunctionParameters = sExecutionID
Call PrintInFunction_Lib(sExecutionID, sUser, sFunctionName, sFunctionParameters)
    
    Call SetInitialData(sExecutionID)

    Call RunFormatAPReconciliation(sExecutionID)
    UiPath_RunFormatAPReconciliation = sExecutionID

    Call UpdateExecutionEndTime_Lib(sExecutionID, sUser, sAutomationID)

sExecutionExitMode = "Traditional"
Call PrintOutFunction_Lib(sExecutionID, sUser, sExecutionExitMode, sFunctionName, sFunctionParameters)

Call UpdateExecutionEndTime_Lib(sExecutionID, sUser, sAutomationID)
Call PrintTestResult_Lib(sExecutionID, sUser)

Application.DisplayAlerts = False
ActiveWorkbook.Close SaveChanges:=True
Application.DisplayAlerts = True

End Function


Sub RunFormatAPReconciliation(sExecutionID)

Dim sSOParameter                        As String
Dim aSOParameters()                     As Variant
Dim aSheetsArray()                      As String
Dim rn                                  As Range
Dim ws                                  As Worksheet
Dim wbMain                              As Workbook
Dim sFunctionName                       As String
Dim sFunctionParameters                 As String
Dim sExecutionExitMode                  As String


sFunctionName = "Sub RunFormatAPReconciliation(sExecutionID)"
sFunctionParameters = sExecutionID
Call PrintInFunction_Lib(sExecutionID, sUser, sFunctionName, sFunctionParameters)

    sHostName = GetHostName_Lib(sExecutionID, sUser)
    aSOParameters = GetOSParametersList_Lib(sExecutionID, sUser)
    Call InsertOSParameters_Lib(sExecutionID, sUser, sHostName, aSOParameters)
    
    sSOParameter = "xlDateOrder"
    sXlDateOrder = GetExecutionAutomationSOParameters_Lib(sExecutionID, sUser, sSOParameter)
    
    If sUser <> "ICARVALHO" Or bolRunSuportFileScripts = True Then
         Call RunAllSuportFileScript(sExecutionID, sUser)
    End If

    Set wbMain = Workbooks(sAutomationMainFileName)
    Call ClearAllNames_Lib(sExecutionID, sUser, wbMain)
    
    Call DeleteSheets(sExecutionID)
        
    aSheetsArray = SetsSheetsArray(sExecutionID)
    Call CreateSheetsFromArray_Lib(sExecutionID, sUser, aSheetsArray)
    Call SetSheetsVariables(sExecutionID, sUser)
    
    aSheetsArray = SetSheetArrayToMove(sExecutionID)
    Call MoveSheets_Lib(sExecutionID, sUser, aSheetsArray)

    Call LoadAllSheetTempData(sExecutionID)
    Call LoadSPL086000030(sExecutionID)
    Call LoadAllExcelData(sExecutionID)
    Call ConciliacaoAtivo(sExecutionID)
    Call ConciliacaoPassivo(sExecutionID)
    Call CheckIsThereVC(sExecutionID)
    Call ConciliacaoVC(sExecutionID)
    Call RefreshDataMatching(sExecutionID)
    Call PreencherCapas(sExecutionID)
    
    Calculate
    
    aSheetsArray = SetsSheetsArray(sExecutionID)
    Call AutoFitEntireColumn_Lib(sExecutionID, sUser, aSheetsArray)
    
    Application.ScreenUpdating = True
    Application.Calculation = xlCalculationAutomatic
    
    Sheets("(Forn) BRA").Select
    
    sTestStatus = "Tests Not Implemented"
    Call UpdateExecTestStatus_Lib(sExecutionID, sUser, sTestStatus)

sExecutionExitMode = "Traditional"
Call PrintOutFunction_Lib(sExecutionID, sUser, sExecutionExitMode, sFunctionName, sFunctionParameters)

End Sub






