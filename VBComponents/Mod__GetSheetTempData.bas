Attribute VB_Name = "Mod__GetSheetTempData"
Option Explicit

Sub LoadAllSheetTempData(sExecutionID)

Dim sFunctionName                   As String
Dim sFunctionParameters             As String
Dim sExecutionExitMode              As String

sFunctionName = "Sub LoadAllSheetTempData(sExecutionID)"
sFunctionParameters = sExecutionID
Call PrintInFunction_Lib(sExecutionID, sUser, sFunctionName, sFunctionParameters)

    wsTemp.Cells.Clear
    
    sSheetName = "Temp"
    sTargetCell = "$A$1"
    Call GetPeriod_Lib(sExecutionID, sUser, sSheetName, sTargetCell, sXlDateOrder)
    
    sSheetName = "Temp"
    sTargetCell = "$E$1"
    sRangeName = "Rate"
    Call GetRateReconciliationCover_Lib(sExecutionID, sUser, iYear, iMonth, sSheetName, sRangeName, sTargetCell)
    Call CheckRateIsOK(sExecutionID, rnRange, sSheetName, sTargetCell)
        
    sSheetName = "Temp"
    sTargetCell = "$J$1"
    sRangeName = "BusinessPlace"
    Call LoadBusinessPlace_Lib(sExecutionID, sUser, sSheetName, sRangeName, sTargetCell)
    
    sIdReconciliationCover = "E7A18D7D-AB07-4E78-B617-850B5BD4E88B"
    sSheetName = "Temp"
    sTargetCell = "$O$1"
    sRangeName = "CoverAccounts"
    Call GetReconciliationCoverAccounts_Lib(sExecutionID, sUser, sIdReconciliationCover, sSheetName, sRangeName, sTargetCell)
    
    Call SpecificFormatTemp(sExecutionID)

sExecutionExitMode = "Traditional"
Call PrintOutFunction_Lib(sExecutionID, sUser, sExecutionExitMode, sFunctionName, sFunctionParameters)

End Sub


