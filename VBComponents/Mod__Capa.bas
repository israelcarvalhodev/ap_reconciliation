Attribute VB_Name = "Mod__Capa"
Option Explicit

Sub PreencherCapas(sExecutionID)
Attribute PreencherCapas.VB_ProcData.VB_Invoke_Func = " \n14"

Dim company                         As String
Dim sFunctionName                   As String
Dim sFunctionParameters             As String
Dim sExecutionExitMode              As String

sFunctionName = "Sub PreencherCapas(sExecutionID)"
sFunctionParameters = sExecutionID
Call PrintInFunction_Lib(sExecutionID, sUser, sFunctionName, sFunctionParameters)

    Sheets("(Forn) BRA").Select
    [V1].FormulaR1C1 = "=USD"
    [U6].FormulaR1C1 = "=dtPeriod"
    [D11:R16].FormulaR1C1 = "=SUMIFS(SPL086000030!C6,SPL086000030!C5,R9C,SPL086000030!C3,RC1)"
    [S11:S16].FormulaR1C1 = "=SUM(RC[-15]:RC[-1])"
    [T11:T16].FormulaR1C1 = "=RC18/R1C22"
    [U11].FormulaR1C1 = "=SUMIFS(ZFI016!C13,ZFI016!C12,""BRL"",ZFI016!C16,""ZNPN"")+" & Chr(10) & "SUMIFS(ZFI016!C13,ZFI016!C12,""BRL"",ZFI016!C16,""ZPNA"")+" & Chr(10) & "SUMIFS(ZFI016!C13,ZFI016!C12,""BRL"",ZFI016!C16,""ZGTX"")"
    [U12].FormulaR1C1 = "=SUMIFS(ZFI016!C13,ZFI016!C12,""BRL"",ZFI016!C16,""ZNPE"")+" & Chr(10) & "SUMIFS(ZFI016!C13,ZFI016!C12,""BRL"",ZFI016!C16,""ZPEX"")"
    [U13].FormulaR1C1 = "=SUMIFS(ZFI016!C13,ZFI016!C12,""BRL"",ZFI016!C16,""ZEMA"")+" & Chr(10) & "SUMIFS(ZFI016!C13,ZFI016!C12,""BRL"",ZFI016!C16,""ZEMP"")"
    [U14].FormulaR1C1 = "=SUMIFS(ZFI016!C13,ZFI016!C12,""BRL"",ZFI016!C16,""ZGET"")+" & Chr(10) & "SUMIFS(ZFI016!C13,ZFI016!C12,""BRL"",ZFI016!C16,""ZBAN"")"
    [U15].FormulaR1C1 = "=SUMIFS(ZFI016!C13,ZFI016!C12,""BRL"",ZFI016!C16,""ZLGN"")"
    [U16].FormulaR1C1 = "=SUMIF(ZFI016!C12,""BRL"",ZFI016!C21)+" & Chr(10) & "SUMIF(FBL1N!C16,""BRL"",FBL1N!C23)"
    [V11:V16].FormulaR1C1 = "=RC21-RC19"
    
    Sheets("(Adia) BRA").Select
    [V1].FormulaR1C1 = "=USD"
    [U6].FormulaR1C1 = "=dtPeriod"
    [D11:R17].FormulaR1C1 = "=SUMIFS(SPL086000030!C6,SPL086000030!C5,R9C,SPL086000030!C3,RC1)"
    [S11:S17].FormulaR1C1 = "=SUM(RC[-15]:RC[-1])"
    [T11:T17].FormulaR1C1 = "=RC19/R1C22"
    [U11].FormulaR1C1 = "=SUMIFS(" & Chr(10) & "FBL1N!C17," & Chr(10) & "FBL1N!C16,""BRL""," & Chr(10) & "FBL1N!C9,RC1)"
    [U12].FormulaR1C1 = "=SUMIFS(" & Chr(10) & "FBL1N!C17," & Chr(10) & "FBL1N!C16,""BRL""," & Chr(10) & "FBL1N!C9,RC1)"
    [U13].FormulaR1C1 = "=SUMIFS(" & Chr(10) & "FBL1N!C17," & Chr(10) & "FBL1N!C16,""BRL""," & Chr(10) & "FBL1N!C9,RC1)"
    [U14].FormulaR1C1 = "=SUMIFS(" & Chr(10) & "FBL1N!C17," & Chr(10) & "FBL1N!C16,""BRL""," & Chr(10) & "FBL1N!C9,RC1)"
    [U15].FormulaR1C1 = "=SUMIFS(" & Chr(10) & "FBL1N!C17," & Chr(10) & "FBL1N!C16,""BRL""," & Chr(10) & "FBL1N!C9,RC1)"
    [U16].FormulaR1C1 = "=SUMIFS(" & Chr(10) & "FBL1N!C17," & Chr(10) & "FBL1N!C16,""BRL""," & Chr(10) & "FBL1N!C9,RC1)"
    [U17].FormulaR1C1 = "=SUMIFS(" & Chr(10) & "FBL1N!C17," & Chr(10) & "FBL1N!C16,""BRL""," & Chr(10) & "FBL1N!C9,RC1)"
    [V11:V17].FormulaR1C1 = "=RC21-RC19"
    
    Sheets("(Forn) ARG").Select
    [K1].FormulaR1C1 = "=ARS"
    [J6].FormulaR1C1 = "=dtPeriod"
    [D11:G14].FormulaR1C1 = "=SUMIFS(SPL086000030!C6,SPL086000030!C5,R9C,SPL086000030!C3,RC1)"
    [H11:H14].FormulaR1C1 = "=SUM(RC[-4]:RC[-1])"
    [I11:I14].FormulaR1C1 = "=RC8/R1C11"
    [J11].FormulaR1C1 = "=SUMIFS(ZFI016!C13,ZFI016!C12,""ARS"",ZFI016!C16,""ZNPN"")+" & Chr(10) & "SUMIFS(ZFI016!C13,ZFI016!C12,""ARS"",ZFI016!C16,""ZPNA"")+" & Chr(10) & "SUMIFS(ZFI016!C13,ZFI016!C12,""ARS"",ZFI016!C16,""ZGTX"")+" & Chr(10) & "SUMIFS(ZFI016!C13,ZFI016!C12,""ARS"",ZFI016!C16,""ZLGN"")+" & Chr(10) & "SUMIFS(ZFI016!C13,ZFI016!C12,""ARS"",ZFI016!C16,""ZNPF"")"
    [J12].FormulaR1C1 = "=SUMIFS(ZFI016!C13,ZFI016!C12,""ARS"",ZFI016!C16,""ZNPE"")+" & Chr(10) & "SUMIFS(ZFI016!C13,ZFI016!C12,""ARS"",ZFI016!C16,""ZPEX"")+" & Chr(10) & "SUMIFS(ZFI016!C13,ZFI016!C12,""ARS"",ZFI016!C16,""ZBAN"")+" & Chr(10) & "SUMIFS(ZFI016!C13,ZFI016!C12,""ARS"",ZFI016!C16,""ZLGE"")"
    [J13].FormulaR1C1 = "=SUMIFS(ZFI016!C13,ZFI016!C12,""ARS"",ZFI016!C16,""ZEMA"")"
    [J14].FormulaR1C1 = "=SUMIF(ZFI016!C12,""ARS"",ZFI016!C21)+" & Chr(10) & "SUMIF(FBL1N!C16,""ARS"",FBL1N!C23)"
    [K11:K14].FormulaR1C1 = "=RC10-RC8"
    
    Sheets("(Adia) ARG").Select
    [K1].FormulaR1C1 = "=ARS"
    [J6].FormulaR1C1 = "=dtPeriod"
    [D11:G17].FormulaR1C1 = "=SUMIFS(SPL086000030!C6,SPL086000030!C5,R9C,SPL086000030!C3,RC1)"
    [H11:H17].FormulaR1C1 = "=SUM(RC[-4]:RC[-1])"
    [I11:I17].FormulaR1C1 = "=RC8/R1C11"
    [J11].FormulaR1C1 = "=SUMIFS(" & Chr(10) & "FBL1N!C17," & Chr(10) & "FBL1N!C16,""ARS""," & Chr(10) & "FBL1N!C9,RC1)"
    [J12].FormulaR1C1 = "=SUMIFS(" & Chr(10) & "FBL1N!C17," & Chr(10) & "FBL1N!C16,""ARS""," & Chr(10) & "FBL1N!C9,RC1)"
    [J13].FormulaR1C1 = "=SUMIFS(" & Chr(10) & "FBL1N!C17," & Chr(10) & "FBL1N!C16,""ARS""," & Chr(10) & "FBL1N!C9,RC1)"
    [J14].FormulaR1C1 = "=SUMIFS(" & Chr(10) & "FBL1N!C17," & Chr(10) & "FBL1N!C16,""ARS""," & Chr(10) & "FBL1N!C9,RC1)"
    [J15].FormulaR1C1 = "=SUMIFS(" & Chr(10) & "FBL1N!C17," & Chr(10) & "FBL1N!C16,""ARS""," & Chr(10) & "FBL1N!C9,RC1)"
    [J16].FormulaR1C1 = "=SUMIFS(" & Chr(10) & "FBL1N!C17," & Chr(10) & "FBL1N!C16,""ARS""," & Chr(10) & "FBL1N!C9,RC1)"
    [J17].FormulaR1C1 = "=SUMIFS(" & Chr(10) & "FBL1N!C17," & Chr(10) & "FBL1N!C16,""ARS""," & Chr(10) & "FBL1N!C9,RC1)"
    [K11:K17].FormulaR1C1 = "=RC10-RC8"


sExecutionExitMode = "Traditional"
Call PrintOutFunction_Lib(sExecutionID, sUser, sExecutionExitMode, sFunctionName, sFunctionParameters)

End Sub

