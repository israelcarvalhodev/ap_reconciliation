Attribute VB_Name = "Mod__ConciliacaoAtivo"
Option Explicit

Sub ConciliacaoAtivo(sExecutionID)

Dim wbMain                          As Workbook
Dim rn                              As Range
Dim wsOutput                        As Worksheet
Dim sFunctionName                   As String
Dim sFunctionParameters             As String
Dim sExecutionExitMode              As String

sFunctionName = "Sub ConciliacaoAtivo(sExecutionID)"
sFunctionParameters = sExecutionID
Call PrintInFunction_Lib(sExecutionID, sUser, sFunctionName, sFunctionParameters)

    wsFAGLL03.Select
    
    Set rn = wsFAGLL03.[A1].CurrentRegion
    Set wsOutput = Sheets("1.1 - Conciliação Ativo")
    
    rn.AutoFilter Field:=7, Criteria1:="=1*", Operator:=xlAnd
    rn.Range([A1], [B2].End(xlDown)).Copy Destination:=wsOutput.[B1] 'Segment / Plant
    rn.Range([G1], [H2].End(xlDown)).Copy Destination:=wsOutput.[E1] ' Account / SAP Account Name
    rn.Range([C1], [C2].End(xlDown)).Copy Destination:=wsOutput.[D1] ' Document Number
    rn.Range([J1], [K2].End(xlDown)).Copy Destination:=wsOutput.[G1] ' Local Currency / Amount
    ActiveSheet.ShowAllData
    
    wsOutput.Select
    Range(wsOutput.[B2], wsOutput.[B2].End(xlDown)).Offset(0, -1).value = "FAGLL03"
    wsOutput.[A1].value = "Fonte"
    wsOutput.[B:B].Copy
    wsOutput.[A:B].PasteSpecial (xlPasteFormats)
    
    '--------------------------------------------------------------------------------------------------------------------------------------------
    
    wsFBL1N.Select
    
    Set rn = wsFBL1N.[A1].CurrentRegion
    
    rn.Range([H2], [H2].End(xlDown)).Copy Destination:=wsOutput.[B1].End(xlDown).Offset(1, 0) ' Segment
    rn.Range([F2], [F2].End(xlDown)).Copy Destination:=wsOutput.[C1].End(xlDown).Offset(1, 0) ' Plant
    rn.Range([I2], [J2].End(xlDown)).Copy Destination:=wsOutput.[E1].End(xlDown).Offset(1, 0) ' Account / SAP Account Name
    rn.Range([B2], [B2].End(xlDown)).Copy Destination:=wsOutput.[D1].End(xlDown).Offset(1, 0) ' Document Number
    rn.Range([Q2], [Q2].End(xlDown)).Copy Destination:=wsOutput.[H1].End(xlDown).Offset(1, 0) ' Amount Local Currency
    rn.Range([P2], [P2].End(xlDown)).Copy Destination:=wsOutput.[G1].End(xlDown).Offset(1, 0) ' Local Currency
    
    wsOutput.Select
    Set rn = wsOutput.[A1].CurrentRegion
    
    wsOutput.Range([A1].End(xlDown).Offset(1, 0), [B2].End(xlDown).Offset(0, -1)).value = "FBL1N"
    wsOutput.Range([A2], [A2].End(xlToRight)).Copy
    wsOutput.[A1].CurrentRegion.Resize(rn.Rows.Count - 1, wsOutput.[A1].CurrentRegion.Columns.Count).Offset(1, 0).PasteSpecial (xlPasteFormats)
    
    '--------------------------------------------------------------------------------------------------------------------------------------------
    Set rn = Sheets("1.1 - Conciliação Ativo").[D2]
    Set wbMain = Workbooks("AP_Reconciliation.xlsm")
    Call SetFreezePanes_Lib(sExecutionID, sUser, wbMain, rn)
    
    Cells.EntireColumn.AutoFit
    ActiveWindow.SmallScroll ToRight:=-100
    ActiveWindow.SmallScroll Down:=-100

sExecutionExitMode = "Traditional"
Call PrintOutFunction_Lib(sExecutionID, sUser, sExecutionExitMode, sFunctionName, sFunctionParameters)

End Sub

