Attribute VB_Name = "Mod__Tests"
Option Explicit

Sub tE_Verificar()

Dim sExecutionID            As String

sExecutionID = "55AD5743-FE03-4AB4-A185-21C91C8D265E"

Call Teste_MR11_18(sExecutionID)

End Sub

Function CheckRateIsOK(sExecutionID, rnRange, sSheetName, sTargetCell)

Dim iTestNumber                     As Integer
Dim sTestDescription                As String
Dim rnStartRange                    As Range
Dim rnEndRange                      As Range
Dim sR1C1StartRange                 As String
Dim sR1C1EndRange                   As String
Dim sExpectedValue                  As String
Dim sRealValue                      As String
Dim bolIsPossibleNullValue          As Boolean
Dim sFunctionName                   As String
Dim sFunctionParameters             As String
Dim sExecutionExitMode              As String

iTestNumber = 18 'N�mero do Teste
sTestDescription = "Check Possible Values"
bolIsPossibleNullValue = False

    sTargetCellR1C1 = Application.ConvertFormula(Formula:=sTargetCell, FromReferenceStyle:=xlA1, ToReferenceStyle:=xlR1C1)
    lLn = GetRowFromStringRange_Lib(sExecutionID, sUser, sTargetCellR1C1)
    iCol = GetColumnFromStringRange_Lib(sExecutionID, sUser, sTargetCellR1C1)
    
    Set rn = Cells(lLn, iCol)
    
    sR1C1StartRange = ConvertRangeToR1C1_Lib(rn.Offset(1, 3))
    sR1C1EndRange = ConvertRangeToR1C1_Lib(rn.Offset(2, 3))
    Set rnStartRange = rn.Offset(1, 2)
    Set rnEndRange = rn.Offset(2, 2)
    Set rnRange = Range(rnStartRange, rnEndRange)
    sExpectedValue = "<> NULL"
    sRealValue = "NULL"
    
    For Each rn In rnRange
        If rn.value = Empty Then
            rn.Select
            Stop
            'Inserir rotina de quebra de teste
            
            Call FailedPatternTests_Lib( _
                 sExecutionID, _
                 sUser, _
                 iTestNumber, _
                 sTestDescription, _
                 sExpectedValue, _
                 sRealValue)
            Exit For
        End If
    Next rn
    
    If sTestStatus = "Failed" Then
        sExecutionExitMode = "Aborted"
        Call PrintOutFunction_Lib(sExecutionID, sUser, sExecutionExitMode, sFunctionName, sFunctionParameters)
        Exit Function
    End If
    
    For Each rn In rnRange
        rn.Select
        Select Case rn.value
            Case "ARS"
                ActiveWorkbook.Names.Add name:="ARS", RefersToR1C1:="=Temp!" & sR1C1StartRange
            Case "USD"
                ActiveWorkbook.Names.Add name:="USD", RefersToR1C1:="=Temp!" & sR1C1EndRange
            Case Else
                Stop
        End Select
    
    Next rn





Call PatternSearchNullValuesInRange_Lib( _
     sExecutionID, _
     iTestNumber, _
     sTestDescription, _
     rnRange, _
     bolIsPossibleNullValue)
     
'sExecutionExitMode = "Traditional"
'Call PrintOutFunction_Lib(sExecutionID,  sUser, sExecutionExitMode, sFunctionName, sFunctionParameters)
        
End Function

Function PatternSearchNullValuesInRange_Lib( _
     sExecutionID, _
     iTestNumber, _
     sTestDescription, _
     rnRange, _
     bolIsPossibleNullValue)

Dim X As Range

For Each X In rnRange

    X.Select

Next X


End Function

