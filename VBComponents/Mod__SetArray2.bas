Attribute VB_Name = "Mod__SetArray2"
Option Explicit

Function SetsSheetsArray(sExecutionID) As String()
  
Dim aArray()                 As String
Dim sFunctionName            As String
Dim sFunctionParameters      As String
Dim sExecutionExitMode       As String

sFunctionName = "Function SetsSheetsArray(sExecutionID) As String()"
sFunctionParameters = sExecutionID
Call PrintInFunction_Lib(sExecutionID, sUser, sFunctionName, sFunctionParameters)

    ReDim aArray(8, 1)
    
    'SheetName
    aArray(0, 0) = "ZFI016"
    aArray(1, 0) = "FBL1N"
    aArray(2, 0) = "FAGLL03"
    aArray(3, 0) = "FAGLFCV"
    aArray(4, 0) = "SPL086000030"
    aArray(5, 0) = "1.1 - Concilia��o Ativo"
    aArray(6, 0) = "2.1 - Concilia��o Passivo"
    aArray(7, 0) = "3.1 - Concilia��o VC"
    aArray(8, 0) = "Temp"
    
    'SheetColor
    aArray(0, 1) = "5296274"
    aArray(1, 1) = "5296274"
    aArray(2, 1) = "5296274"
    aArray(3, 1) = "5296274"
    aArray(4, 1) = "5296274"
    aArray(5, 1) = "11892015"
    aArray(6, 1) = "1137094"
    aArray(7, 1) = "5395026"
    aArray(8, 1) = "000000"
    
    SetsSheetsArray = aArray

sExecutionExitMode = "Traditional"
Call PrintOutFunction_Lib(sExecutionID, sUser, sExecutionExitMode, sFunctionName, sFunctionParameters)

End Function


Function SetSheetArrayToMove(sExecutionID) As String()
  
Dim aArray()                 As String
Dim sFunctionName            As String
Dim sFunctionParameters      As String
Dim sExecutionExitMode       As String

sFunctionName = "Function SetSheetArrayToMove(sExecutionID) As String()"
sFunctionParameters = sExecutionID
Call PrintInFunction_Lib(sExecutionID, sUser, sFunctionName, sFunctionParameters)

    'Arrays utilizados para movimenta��o de Sheets dentro do Workbook
    
    ReDim aArray(18, 1)
      
    
    aArray(0, 0) = "Documentation"  'Nome da guia que ser� movida
    aArray(0, 1) = 0                'Nome da guia onde ap�s seu index, o destino da sheet que ser� movida
        
    aArray(1, 0) = "ZFI016"
    aArray(1, 1) = "Documentation"
    
    aArray(2, 0) = "FBL1N"
    aArray(2, 1) = "ZFI016"
    
    aArray(3, 0) = "FAGLL03"
    aArray(3, 1) = "FBL1N"
    
    aArray(4, 0) = "FAGLFCV"
    aArray(4, 1) = "FAGLL03"
    
    aArray(5, 0) = "SPL086000030"
    aArray(5, 1) = "FAGLFCV"
    
    aArray(6, 0) = "Resumo Aging BRA"
    aArray(6, 1) = "SPL086000030"
    
    aArray(7, 0) = "Resumo Aging ARG"
    aArray(7, 1) = "Resumo Aging BRA"
    
    aArray(8, 0) = "1.1 - Concilia��o Ativo"
    aArray(8, 1) = "Resumo Aging ARG"
    
    aArray(9, 0) = "1.2 - PT_Ativo"
    aArray(9, 1) = "1.1 - Concilia��o Ativo"
    
    aArray(10, 0) = "2.1 - Concilia��o Passivo"
    aArray(10, 1) = "1.2 - PT_Ativo"
    
    aArray(11, 0) = "2.2 - PT_Passivo"
    aArray(11, 1) = "2.1 - Concilia��o Passivo"
    
    aArray(12, 0) = "3.1 - Concilia��o VC"
    aArray(12, 1) = "2.2 - PT_Passivo"
    
    aArray(13, 0) = "3.2 - PT_Varia��o Cambial"
    aArray(13, 1) = "3.1 - Concilia��o VC"
    
    aArray(14, 0) = "Temp"
    aArray(14, 1) = "3.2 - PT_Varia��o Cambial"
    
    aArray(15, 0) = "(Forn) BRA"
    aArray(15, 1) = "Temp"
    
    aArray(16, 0) = "(Adia) BRA"
    aArray(16, 1) = "(Forn) BRA"
    
    aArray(17, 0) = "(Forn) ARG"
    aArray(17, 1) = "(Adia) BRA"
    
    aArray(18, 0) = "(Adia) ARG"
    aArray(18, 1) = "(Forn) ARG"
    
    SetSheetArrayToMove = aArray

sExecutionExitMode = "Traditional"
Call PrintOutFunction_Lib(sExecutionID, sUser, sExecutionExitMode, sFunctionName, sFunctionParameters)

End Function

Function SetsSheetsArrayToOpenSuportFile(sExecutionID) As String()
  
Dim aArray()                 As String
Dim sFunctionName            As String
Dim sFunctionParameters      As String
Dim sExecutionExitMode       As String

sFunctionName = "Function SetsSheetsArrayToOpenSuportFile(sExecutionID) As String()"
sFunctionParameters = sExecutionID
Call PrintInFunction_Lib(sExecutionID, sUser, sFunctionName, sFunctionParameters)

    ReDim aArray(3, 2)
    
    'Sheet Name
    aArray(0, 0) = "ZFI016"
    aArray(1, 0) = "FBL1N"
    aArray(2, 0) = "FAGLL03"
    aArray(3, 0) = "FAGLFCV"
    
    'Automation Folder
    aArray(0, 1) = "zfi016_vendor"
    aArray(1, 1) = "fbl1n"
    aArray(2, 1) = "fagll03_vendor"
    aArray(3, 1) = "faglfcv_vendor"
    
    'Suport File Name
    aArray(0, 2) = "ZFI016.xlsm"
    aArray(1, 2) = "FBL1N.xlsm"
    aArray(2, 2) = "FAGLL03.xlsm"
    aArray(3, 2) = "FAGLFCV.xlsm"

    
    SetsSheetsArrayToOpenSuportFile = aArray

sExecutionExitMode = "Traditional"
Call PrintOutFunction_Lib(sExecutionID, sUser, sExecutionExitMode, sFunctionName, sFunctionParameters)

End Function


