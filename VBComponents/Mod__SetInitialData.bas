Attribute VB_Name = "Mod__SetInitialData"
Option Explicit

Sub SetInitialData(sExecutionID)

Dim sFunctionName            As String
Dim sFunctionParameters      As String
Dim sExecutionExitMode       As String

sFunctionName = "Sub Sub SetInitialData(sExecutionID)"
sFunctionParameters = sExecutionID
Call PrintInFunction_Lib(sExecutionID, sUser, sFunctionName, sFunctionParameters)

    sAutomationsRootPath = GetRootPath_Lib(sExecutionID, sUser)
    sAutomationGitRepository = GetAutomationGitRepository_Lib(sAutomationID, sUser)
    sOriginalFilesPath = sAutomationsRootPath & sAutomationGitRepository & "\" & cOriginalFilesFolder

sExecutionExitMode = "Traditional"
Call PrintOutFunction_Lib(sExecutionID, sUser, sExecutionExitMode, sFunctionName, sFunctionParameters)

End Sub
