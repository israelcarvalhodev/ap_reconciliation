Attribute VB_Name = "Mod__ConciliacaoPassivo"
Option Explicit

Sub ConciliacaoPassivo(sExecutionID)

Dim wbMain                          As Workbook
Dim rn                              As Range
Dim wsOutput                        As Worksheet
Dim sFunctionName                   As String
Dim sFunctionParameters             As String
Dim sExecutionExitMode              As String

sFunctionName = "Sub ConciliacaoPassivo(sExecutionID)"
sFunctionParameters = sExecutionID
Call PrintInFunction_Lib(sExecutionID, sUser, sFunctionName, sFunctionParameters)

    wsZFI016.Select
    
    Set rn = wsZFI016.[A1].CurrentRegion
    Set wsOutput = Sheets("2.1 - Conciliação Passivo")
    
    rn.Range([D1], [D2].End(xlDown)).Copy Destination:=wsOutput.[B1]
    rn.Range([E1], [E2].End(xlDown)).Copy Destination:=wsOutput.[C1]
    rn.Range([A1], [A2].End(xlDown)).Copy Destination:=wsOutput.[D1]
    rn.Range([R1], [S2].End(xlDown)).Copy Destination:=wsOutput.[E1]
    rn.Range([L1], [M2].End(xlDown)).Copy Destination:=wsOutput.[G1]
    
    wsOutput.Select
    Range(wsOutput.[B2], wsOutput.[B2].End(xlDown)).Offset(0, -1).value = "ZFI016"
    wsOutput.[A1].value = "Fonte"
    wsOutput.Range([B1], [B2].End(xlDown)).Copy
    wsOutput.[A1].PasteSpecial (xlPasteFormats)
    
    '--------------------------------------------------------------------------------------------------------------------------------------------
    
    wsFAGLL03.Select
    
    Set rn = wsFAGLL03.[A1].CurrentRegion
    
    rn.AutoFilter Field:=7, Criteria1:="=2*", Operator:=xlAnd
    rn.AutoFilter Field:=8, Criteria1:="<>Var Cambial Forneced", Operator:=xlAnd
    rn.Range([A2], [B2].End(xlDown)).Copy Destination:=wsOutput.[B500000].End(xlUp).Offset(1, 0) 'Segment / Plant
    rn.Range([G2], [H2].End(xlDown)).Copy Destination:=wsOutput.[E500000].End(xlUp).Offset(1, 0) ' Account / SAP Account Name
    rn.Range([C2], [C2].End(xlDown)).Copy Destination:=wsOutput.[D500000].End(xlUp).Offset(1, 0) ' Document Number
    rn.Range([J2], [K2].End(xlDown)).Copy Destination:=wsOutput.[G500000].End(xlUp).Offset(1, 0) ' Local Currency / Amount
    ActiveSheet.ShowAllData
    
    wsOutput.Select
    Set rn = wsOutput.[A1].CurrentRegion
    
    wsOutput.Range([A1].End(xlDown).Offset(1, 0), [B2].End(xlDown).Offset(0, -1)).value = "FAGLL03"
    wsOutput.Range([A2], [A2].End(xlToRight)).Copy
    wsOutput.[A1].CurrentRegion.Resize(rn.Rows.Count - 1, wsOutput.[A1].CurrentRegion.Columns.Count).Offset(1, 0).PasteSpecial (xlPasteFormats)
    
    Set rn = Sheets("2.1 - Conciliação Passivo").[D2]
    Set wbMain = Workbooks("AP_Reconciliation.xlsm")
    Call SetFreezePanes_Lib(sExecutionID, sUser, wbMain, rn)
        
    Cells.EntireColumn.AutoFit
    ActiveWindow.SmallScroll ToRight:=-100
    ActiveWindow.SmallScroll Down:=-100

sExecutionExitMode = "Traditional"
Call PrintOutFunction_Lib(sExecutionID, sUser, sExecutionExitMode, sFunctionName, sFunctionParameters)

End Sub

