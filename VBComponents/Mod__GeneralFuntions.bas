Attribute VB_Name = "Mod__GeneralFuntions"
Option Explicit

Function SetAutomationID() As String

SetAutomationID = "EC7C9087-8086-4315-9BC1-9510FCF3848E"
'Select * From T_Automations Where [ID] - 'EC7C9087-8086-4315-9BC1-9510FCF3848E'

End Function

Sub CheckIsThereVC(sExecutionID)

Dim sFunctionName                   As String
Dim sFunctionParameters             As String
Dim sExecutionExitMode              As String

sFunctionName = "Sub CheckIsThereVC(sExecutionID)"
sFunctionParameters = sExecutionID
Call PrintInFunction_Lib(sExecutionID, sUser, sFunctionName, sFunctionParameters)

    boltemVcZFI016 = CheckIsThereVcZFI016(sExecutionID)
    boltemVcFBL1N = CheckIsThereVcFBL1N(sExecutionID)
    boltemVcReavaliacaoCambial = CheckIsThereVcReavaliacaoCambial(sExecutionID)

sExecutionExitMode = "Traditional"
Call PrintOutFunction_Lib(sExecutionID, sUser, sExecutionExitMode, sFunctionName, sFunctionParameters)

End Sub

Function CheckIsThereVcZFI016(sExecutionID) As Boolean

Dim sFunctionName                   As String
Dim sFunctionParameters             As String
Dim sExecutionExitMode              As String

sFunctionName = "Function CheckIsThereVcZFI016(sExecutionID) As Boolean"
sFunctionParameters = sExecutionID
Call PrintInFunction_Lib(sExecutionID, sUser, sFunctionName, sFunctionParameters)
    
    wsZFI016.Select
    
    On Error Resume Next
        ActiveSheet.ShowAllData
    On Error GoTo 0
        
    wsZFI016.[A1].CurrentRegion.AutoFilter Field:=21, Criteria1:=">0", Operator:=xlOr, Criteria2:="<0"
    
    If ActiveSheet.AutoFilter.Range.Columns(1).SpecialCells(xlCellTypeVisible).Cells.Count - 1 >= 1 Then
        CheckIsThereVcZFI016 = True
    Else
        CheckIsThereVcZFI016 = False
    End If
    
    On Error Resume Next
        ActiveSheet.ShowAllData
    On Error GoTo 0

sExecutionExitMode = "Traditional"
Call PrintOutFunction_Lib(sExecutionID, sUser, sExecutionExitMode, sFunctionName, sFunctionParameters)

End Function

Function CheckIsThereVcFBL1N(sExecutionID) As Boolean

Dim sFunctionName                   As String
Dim sFunctionParameters             As String
Dim sExecutionExitMode              As String

sFunctionName = "Function CheckIsThereVcFBL1N(sExecutionID) As Boolean"
sFunctionParameters = sExecutionID
Call PrintInFunction_Lib(sExecutionID, sUser, sFunctionName, sFunctionParameters)
    
    wsFBL1N.Select
    
    On Error Resume Next
        ActiveSheet.ShowAllData
    On Error GoTo 0
    
    wsFBL1N.[A1].CurrentRegion.AutoFilter Field:=23, Criteria1:=">0", Operator:=xlOr, Criteria2:="<0"
    
    If ActiveSheet.AutoFilter.Range.Columns(1).SpecialCells(xlCellTypeVisible).Cells.Count - 1 >= 1 Then
        CheckIsThereVcFBL1N = True
    Else
        CheckIsThereVcFBL1N = False
    End If
    
    On Error Resume Next
        ActiveSheet.ShowAllData
    On Error GoTo 0
    
sExecutionExitMode = "Traditional"
Call PrintOutFunction_Lib(sExecutionID, sUser, sExecutionExitMode, sFunctionName, sFunctionParameters)

End Function

Function CheckIsThereVcReavaliacaoCambial(sExecutionID) As Boolean

Dim sFunctionName                   As String
Dim sFunctionParameters             As String
Dim sExecutionExitMode              As String

sFunctionName = "Function CheckIsThereVcReavaliacaoCambial(sExecutionID) As Boolean"
sFunctionParameters = sExecutionID
Call PrintInFunction_Lib(sExecutionID, sUser, sFunctionName, sFunctionParameters)
    
    wsFAGLFCV.Select
    
    On Error Resume Next
        ActiveSheet.ShowAllData
    On Error GoTo 0
    
    If wsFAGLFCV.[A2].value <> "" Then
        CheckIsThereVcReavaliacaoCambial = True
    Else
        CheckIsThereVcReavaliacaoCambial = False
    End If
        
    On Error Resume Next
        ActiveSheet.ShowAllData
    On Error GoTo 0

sExecutionExitMode = "Traditional"
Call PrintOutFunction_Lib(sExecutionID, sUser, sExecutionExitMode, sFunctionName, sFunctionParameters)

End Function

Sub DeleteSheets(sExecutionID)

Dim sFunctionName                   As String
Dim sFunctionParameters             As String
Dim sExecutionExitMode              As String

sFunctionName = "Sub DeleteSheets(sExecutionID)"
sFunctionParameters = sExecutionID
Call PrintInFunction_Lib(sExecutionID, sUser, sFunctionName, sFunctionParameters)

    ReDim aSheetsAPreservar(9)
    aSheetsAPreservar(0) = "Documentation"
    aSheetsAPreservar(1) = "Resumo Aging BRA"
    aSheetsAPreservar(2) = "Resumo Aging ARG"
    aSheetsAPreservar(3) = "1.2 - PT_Ativo"
    aSheetsAPreservar(4) = "2.2 - PT_Passivo"
    aSheetsAPreservar(5) = "3.2 - PT_Varia��o Cambial"
    aSheetsAPreservar(6) = "(Forn) BRA"
    aSheetsAPreservar(7) = "(Adia) BRA"
    aSheetsAPreservar(8) = "(Forn) ARG"
    aSheetsAPreservar(9) = "(Adia) ARG"
    
    Call DeleteSheetsExcept2_Lib(sExecutionID, sUser, aSheetsAPreservar)

sExecutionExitMode = "Traditional"
Call PrintOutFunction_Lib(sExecutionID, sUser, sExecutionExitMode, sFunctionName, sFunctionParameters)

End Sub

Sub LoadGeneralData(sExecutionID, wsTemp)

Dim CountOfRecords                  As Integer
Dim sFunctionName                   As String
Dim sFunctionParameters             As String
Dim sExecutionExitMode              As String


sFunctionName = "Sub LoadGeneralData(sExecutionID, wsTemp)"
sFunctionParameters = sExecutionID & ", SHEET " & wsTemp.name
Call PrintInFunction_Lib(sExecutionID, sUser, sFunctionName, sFunctionParameters)

    wsTemp.[A1].value = "Period"
    wsTemp.[B1].value = "Year"
    wsTemp.[C1].value = "Month"
    wsTemp.[A2].value = dtPeriod
    wsTemp.[B2].value = iYear
    wsTemp.[C2].value = iMonth
    
    ActiveWorkbook.Names.Add name:="dtPeriod", RefersToR1C1:="=Temp!R2C1"
    ActiveWorkbook.Names.Add name:="iYear", RefersToR1C1:="=Temp!R2C2"
    ActiveWorkbook.Names.Add name:="iMonth", RefersToR1C1:="=Temp!R2C3"
    
    Set rn = wsTemp.[A1].CurrentRegion
   'Set rn = rnTargetCell.CurrentRegion
    Set rnHeader = GetHeader_Lib(sExecutionID, sUser, rn)
    Set rnWithoutHeader = GetRangeWithoutHeader_Lib(sExecutionID, sUser, rn)
    
    Call FormatGenericTable_Lib(sExecutionID, sUser, rn, rnHeader, rnWithoutHeader)

sExecutionExitMode = "Traditional"
Call PrintOutFunction_Lib(sExecutionID, sUser, sExecutionExitMode, sFunctionName, sFunctionParameters)

End Sub

Sub SetSheetsVariables(sExecutionID, sUser)

Dim sFunctionName            As String
Dim sFunctionParameters      As String
Dim sExecutionExitMode       As String

sFunctionName = "Sub SetSheetsVariables(sExecutionID, sUser)"
sFunctionParameters = sExecutionID & ", " & sUser
Call PrintInFunction_Lib(sExecutionID, sUser, sFunctionName, sFunctionParameters)

    Set wsTemp = Sheets("Temp")
    Set wsZFI016 = Sheets("ZFI016")
    Set wsFAGLL03 = Sheets("FAGLL03")
    Set wsFBL1N = Sheets("FBL1N")
    Set wsFAGLFCV = Sheets("FAGLFCV")
    Set wsSPL086000030 = Sheets("SPL086000030")

sExecutionExitMode = "Traditional"
Call PrintOutFunction_Lib(sExecutionID, sUser, sExecutionExitMode, sFunctionName, sFunctionParameters)

End Sub


