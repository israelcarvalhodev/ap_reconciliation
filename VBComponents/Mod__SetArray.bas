Attribute VB_Name = "Mod__SetArray"
Option Explicit

Function SetArray_Anterio(sExecutionID)
  
Dim sFunctionName            As String
Dim sFunctionParameters      As String
Dim sExecutionExitMode       As String

sFunctionName = "SetArray(sExecutionID)"
sFunctionParameters = sExecutionID
Call PrintInFunction_Lib(sExecutionID, sUser, sFunctionName, sFunctionParameters)

Stop

    ReDim aAccountPayable(7, 4)
    
    'iDefaultArraySize = UBound(aAccountPayable)
    
    'SheetName
    aAccountPayable(0, 0) = "SPL086000030"
    aAccountPayable(1, 0) = "ZFI016"
    aAccountPayable(2, 0) = "FAGLL03"
    aAccountPayable(3, 0) = "FBL1N"
    aAccountPayable(4, 0) = "FAGLFCV"
    aAccountPayable(5, 0) = "1.1 - Concilia��o Ativo"
    aAccountPayable(6, 0) = "2.1 - Concilia��o Passivo"
    aAccountPayable(7, 0) = "3.1 - Concilia��o VC"
    
    'Sheets Position
    aAccountPayable(0, 1) = 1
    aAccountPayable(1, 1) = 2
    aAccountPayable(2, 1) = 3
    aAccountPayable(3, 1) = 4
    aAccountPayable(4, 1) = 5
    aAccountPayable(5, 1) = 6
    aAccountPayable(6, 1) = 8
    aAccountPayable(7, 1) = 10
    
    'Arquivos de Supporte (Conciliar os index desta dimenss�o (2) com os nomes das sheets da dimenss�o 1
    'Ex: aAccountPayable(1, 0) = "ZFI016" ==> aAccountPayable(1, 2) = "ZFI016.xlsm" (ambos na posi��o 1)
    
    aAccountPayable(1, 2) = "ZFI016.xlsm"
    aAccountPayable(2, 2) = "FAGLL03.xlsm"
    aAccountPayable(3, 2) = "FBL1N.xlsm"
    aAccountPayable(4, 2) = "FAGLFCV.xlsm"
    
    'Nome da macro MAIN dos arquivos de suporte
    'aAccountPayable(1, 3) = "'ZFI016.xlsm'!FormatAPReconciliation"
    'aAccountPayable(2, 3) = "'FAGLL03.xlsm'!FormatFAGLL03"
    'aAccountPayable(3, 3) = "'FBL1N.xlsm'!FormatFBL1N"
    'aAccountPayable(4, 3) = "'FAGLFCV.xlsm'!FormatFAGLFCV"

    'Automation Folder
    aAccountPayable(1, 4) = "zfi016_vendor"
    aAccountPayable(2, 4) = "fagll03_vendor"
    aAccountPayable(3, 4) = "fbl1n"
    aAccountPayable(4, 4) = "faglfcv_vendor"
    
sExecutionExitMode = "Traditional"
Call PrintOutFunction_Lib(sExecutionID, sUser, sExecutionExitMode, sFunctionName, sFunctionParameters)

End Function

Function SetSheetsArrayToCreateDelete_Anterio(sExecutionID)

Dim sFunctionName                   As String
Dim sFunctionParameters             As String
Dim sExecutionExitMode              As String

Stop

sFunctionName = "SetSheetsArrayToCreateDelete(sExecutionID)"
sFunctionParameters = sExecutionID
Call PrintInFunction_Lib(sExecutionID, sUser, sFunctionName, sFunctionParameters)
    
    ReDim aOrigemSheet(13)
    ReDim aTargetSheet(13)
    
    ReDim aArray(8, 4)
    
    aArray(0, 0) = "ZFI016"
    aArray(1, 0) = "FBL1N"
    aArray(2, 0) = "FAGLL03"
    aArray(3, 0) = "FAGLFCV"
    aArray(4, 0) = "SPL086000030"
    aArray(5, 0) = "1.1 - Concilia��o Ativo"
    aArray(6, 0) = "2.1 - Concilia��o Passivo"
    aArray(7, 0) = "3.1 - Concilia��o VC"
    aArray(8, 0) = "Temp"
    
    ''Cor das Sheets
    ''15773696 Azul Claro
    ''5296274  Verde
    ''5395026  Cinza
    ''1137094  Ferrugem
    ''11892015 Azul Escuro
    ''000000   Preto
    
    aArray(0, 4) = "5296274"
    aArray(1, 4) = "5296274"
    aArray(2, 4) = "5296274"
    aArray(3, 4) = "5296274"
    aArray(4, 4) = "5296274"
    aArray(5, 4) = "11892015"
    aArray(6, 4) = "1137094"
    aArray(7, 4) = "5395026"
    aArray(8, 4) = "000000"

sExecutionExitMode = "Traditional"
Call PrintOutFunction_Lib(sExecutionID, sUser, sExecutionExitMode, sFunctionName, sFunctionParameters)

End Function


Function SetSheetsArrayToMove_Anterio(sExecutionID)

Stop

Dim sFunctionName                   As String
Dim sFunctionParameters             As String
Dim sExecutionExitMode              As String

sFunctionName = "SetSheetsArrayToMove(sExecutionID)"
sFunctionParameters = sExecutionID
Call PrintInFunction_Lib(sExecutionID, sUser, sFunctionName, sFunctionParameters)
    
    ReDim aOrigemSheet(18)
    ReDim aTargetSheet(18)
    
    aOrigemSheet(0) = "Documentation"  'Nome da guia que ser� movida
    aTargetSheet(0) = 0                'Nome da guia onde ap�s seu index, o destino da sheet que ser� movida
        
    aOrigemSheet(1) = "ZFI016"
    aTargetSheet(1) = "Documentation"
    
    aOrigemSheet(2) = "FBL1N"
    aTargetSheet(2) = "ZFI016"
    
    aOrigemSheet(3) = "FAGLL03"
    aTargetSheet(3) = "FBL1N"
    
    aOrigemSheet(4) = "FAGLFCV"
    aTargetSheet(4) = "FAGLL03"
    
    aOrigemSheet(5) = "SPL086000030"
    aTargetSheet(5) = "FAGLFCV"
    
    aOrigemSheet(6) = "Resumo Aging BRA"
    aTargetSheet(6) = "SPL086000030"
    
    aOrigemSheet(7) = "Resumo Aging ARG"
    aTargetSheet(7) = "Resumo Aging BRA"
    
    aOrigemSheet(8) = "1.1 - Concilia��o Ativo"
    aTargetSheet(8) = "Resumo Aging ARG"
    
    aOrigemSheet(9) = "1.2 - PT_Ativo"
    aTargetSheet(9) = "1.1 - Concilia��o Ativo"
    
    aOrigemSheet(10) = "2.1 - Concilia��o Passivo"
    aTargetSheet(10) = "1.2 - PT_Ativo"
    
    aOrigemSheet(11) = "2.2 - PT_Passivo"
    aTargetSheet(11) = "2.1 - Concilia��o Passivo"
    
    aOrigemSheet(12) = "3.1 - Concilia��o VC"
    aTargetSheet(12) = "2.2 - PT_Passivo"
    
    aOrigemSheet(13) = "3.2 - PT_Varia��o Cambial"
    aTargetSheet(13) = "3.1 - Concilia��o VC"
    
    aOrigemSheet(14) = "Temp"
    aTargetSheet(14) = "3.2 - PT_Varia��o Cambial"
    
    aOrigemSheet(15) = "(Forn) BRA"
    aTargetSheet(15) = "Temp"
    
    aOrigemSheet(16) = "(Adia) BRA"
    aTargetSheet(16) = "(Forn) BRA"
    
    aOrigemSheet(17) = "(Forn) ARG"
    aTargetSheet(17) = "(Adia) BRA"
    
    aOrigemSheet(18) = "(Adia) ARG"
    aTargetSheet(18) = "(Forn) ARG"

sExecutionExitMode = "Traditional"
Call PrintOutFunction_Lib(sExecutionID, sUser, sExecutionExitMode, sFunctionName, sFunctionParameters)



End Function
