Attribute VB_Name = "Mod__GetExcelData"
Option Explicit

Sub LoadAllExcelData(sExecutionID)

Dim wb                                  As Workbook
Dim sSheetName                          As String
Dim sAutomationFolder                   As String
Dim sSuportFileName                     As String
Dim aSheetsArray()                      As String
Dim sFunctionName                       As String
Dim sFunctionParameters                 As String
Dim sExecutionExitMode                  As String

sFunctionName = "Sub LoadAllExcelData(sExecutionID)"
sFunctionParameters = sExecutionID
Call PrintInFunction_Lib(sExecutionID, sUser, sFunctionName, sFunctionParameters)

Set wb = Workbooks("AP_Reconciliation.xlsm")

    aSheetsArray = SetsSheetsArrayToOpenSuportFile(sExecutionID)
    
    For i = 0 To UBound(aSheetsArray)

    sSheetName = aSheetsArray(i, 0)
    sAutomationFolder = aSheetsArray(i, 1)
    sSuportFileName = aSheetsArray(i, 2)

        ChDir sAutomationsRootPath
        'Debug.Print sAutomationsRootPath & sAutomationFolder & "\" & sSuportFileName
       
        Workbooks.OpenText Filename:=sAutomationsRootPath & sAutomationFolder & "\" & sSuportFileName

        Windows(sSuportFileName).Activate

        Sheets("Database").Select
        On Error Resume Next
            ActiveSheet.ShowAllData
        On Error GoTo 0
        
        If [A2].value <> "" Then
            [A1].CurrentRegion.Copy Destination:=wb.Sheets(sSheetName).[A1]
        Else
            [1:1].Copy Destination:=wb.Sheets(sSheetName).[A1]
        End If
        
        Windows(sSuportFileName).Activate
        Application.DisplayAlerts = False
        ActiveWorkbook.Close SaveChanges:=False
        Application.DisplayAlerts = True
        
        If wb.Sheets(sSheetName).[A2].value <> "" Then
        
            Set rn = Sheets(sSheetName).[A1].CurrentRegion
            Set rnHeader = GetHeader_Lib(sExecutionID, sUser, rn)
            Set rnWithoutHeader = GetRangeWithoutHeader_Lib(sExecutionID, sUser, rn)
            Call FormatGenericTable_Lib(sExecutionID, sUser, rn, rnHeader, rnWithoutHeader)

        ElseIf wb.Sheets(sSheetName).[A1].value = "" And _
                wb.Sheets(sSheetName).[A2].value <> "" Then
        
            Call FormatGenericHeaderRange_Lib(sExecutionID, sUser, rnHeader)
        
        End If
    
    Next

sExecutionExitMode = "Traditional"
Call PrintOutFunction_Lib(sExecutionID, sUser, sExecutionExitMode, sFunctionName, sFunctionParameters)

End Sub




