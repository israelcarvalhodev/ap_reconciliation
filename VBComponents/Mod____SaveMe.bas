Attribute VB_Name = "Mod____SaveMe"
Option Explicit

Sub SaveMe()

ThisWorkbook.Save

End Sub

Function ExportVbComponents()

Dim wb                          As Workbook
Dim objVBPrj                    As VBIDE.VBProject
Dim sVBComponentsPath           As String
Dim sGitRepositoryPath          As String
Dim sFilesToBeKilled            As String

sAutomationID = SetAutomationID
sUser = LCase(GetUserName_Lib)
sAutomationsRootPath = "C:\Users\" & sUser & cDevDefaultPath
sAutomationGitRepository = GetAutomationGitRepository_Lib(sAutomationID, sUser)
sVBComponentsPath = sAutomationsRootPath & sAutomationGitRepository & "\" & cVBComponentsFolder
sFilesToBeKilled = sVBComponentsPath & "\*.*"

On Error Resume Next
    Kill sFilesToBeKilled
On Error GoTo 0

Set wb = ThisWorkbook
Set objVBPrj = wb.VBProject
Call ExportVbComponents_Lib(wb, sVBComponentsPath, objVBPrj)

End Function





