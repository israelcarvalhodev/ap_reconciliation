Attribute VB_Name = "Mod__GetDataSqlServer"
Option Explicit

Sub LoadSPL086000030(sExecutionID)

Dim sFunctionName                   As String
Dim sFunctionParameters             As String
Dim sExecutionExitMode              As String

sFunctionName = "Sub LoadSPL086000030(sExecutionID)"
sFunctionParameters = sExecutionID
Call PrintInFunction_Lib(sExecutionID, sUser, sFunctionName, sFunctionParameters)
    
    sIdReconciliationCover = "E7A18D7D-AB07-4E78-B617-850B5BD4E88B"
    sSheetName = "SPL086000030"
    sTargetCell = "$A$1"
    sRangeName = "SPL086000030"
    Call LoadSPL086000030_Lib(sExecutionID, sUser, sIdReconciliationCover, sSheetName, sRangeName, sTargetCell)
    
    Call SpecificFormatSPL086000030(sExecutionID)

sExecutionExitMode = "Traditional"
Call PrintOutFunction_Lib(sExecutionID, sUser, sExecutionExitMode, sFunctionName, sFunctionParameters)

End Sub



