Attribute VB_Name = "Mod__ConciliacaoVC"
Option Explicit

Sub ConciliacaoVC(sExecutionID)

Dim rn                              As Range
Dim wbMain                          As Workbook
Dim ws                              As Worksheet
Dim sFunctionName                   As String
Dim sFunctionParameters             As String
Dim sExecutionExitMode              As String

sFunctionName = "Sub ConciliacaoVC(sExecutionID)"
sFunctionParameters = sExecutionID
Call PrintInFunction_Lib(sExecutionID, sUser, sFunctionName, sFunctionParameters)

    If boltemVcFBL1N = True Or boltemVcZFI016 = True Or boltemVcReavaliacaoCambial = True Then
    
        Set ws = Sheets("3.1 - Conciliação VC")
   
        ws.Select
       
        With ws
            .[A1].value = "Fonte"
            .[B1].value = "Segment"
            .[C1].value = "Plant"
            .[D1].value = "Nº doc."
            .[E1].value = "Acount"
            .[F1].value = "Acount Name"
            .[G1].value = "Local Currency"
            .[H1].value = "Exchange Rate Variation"
        End With
    
        Call LoadZFI016Records(sExecutionID)
        Call LoadFBL1NRecords(sExecutionID)
        Call LoadReavaliacaoCambialRecords(sExecutionID)
    
        Application.ScreenUpdating = True
        Set rn = Sheets("3.1 - Conciliação VC").[B2]
        Set wbMain = Workbooks("AP_Reconciliation.xlsm")
        Call SetFreezePanes_Lib(sExecutionID, sUser, wbMain, rn)
      
        Set rn = ws.[A1].CurrentRegion
        Set rnHeader = GetHeader_Lib(sExecutionID, sUser, rn)
        Set rnWithoutHeader = GetRangeWithoutHeader_Lib(sExecutionID, sUser, rn)
        
        Call FormatGenericTable_Lib(sExecutionID, sUser, rn, rnHeader, rnWithoutHeader)
    
    End If
    
sExecutionExitMode = "Traditional"
Call PrintOutFunction_Lib(sExecutionID, sUser, sExecutionExitMode, sFunctionName, sFunctionParameters)

End Sub

Sub LoadZFI016Records(sExecutionID)

Dim rn                              As Range
Dim wsOutput                        As Worksheet
Dim sFunctionName                   As String
Dim sFunctionParameters             As String
Dim sExecutionExitMode              As String
    
    sFunctionName = "Sub LoadZFI016Records(sExecutionID)"
    sFunctionParameters = sExecutionID
    Call PrintInFunction_Lib(sExecutionID, sUser, sFunctionName, sFunctionParameters)
    
    If boltemVcZFI016 = True Then
    
        wsZFI016.Select
        Set rn = wsZFI016.[A1].CurrentRegion
        Set wsOutput = Sheets("3.1 - Conciliação VC")
        On Error Resume Next
        ActiveSheet.ShowAllData
        On Error GoTo 0
        
        rn.AutoFilter Field:=21, Criteria1:=">0", Operator:=xlOr, Criteria2:="<0"
        
        rn.Range([D2], [D50000].End(xlUp)).SpecialCells(xlCellTypeVisible).Copy Destination:=wsOutput.[B10000].End(xlUp).Offset(1, 0)   'Segment
        rn.Range([E2], [E50000].End(xlUp)).SpecialCells(xlCellTypeVisible).Copy Destination:=wsOutput.[C10000].End(xlUp).Offset(1, 0)   'Plant
        rn.Range([R2], [S50000].End(xlUp)).SpecialCells(xlCellTypeVisible).Copy Destination:=wsOutput.[E10000].End(xlUp).Offset(1, 0)   'Account / SAP Account Name
        rn.Range([A2], [A50000].End(xlUp)).SpecialCells(xlCellTypeVisible).Copy Destination:=wsOutput.[D10000].End(xlUp).Offset(1, 0)   'Document Number
        rn.Range([L2], [L50000].End(xlUp)).SpecialCells(xlCellTypeVisible).Copy Destination:=wsOutput.[G10000].End(xlUp).Offset(1, 0)   'Local Currency
        rn.Range([U2], [U50000].End(xlUp)).SpecialCells(xlCellTypeVisible).Copy Destination:=wsOutput.[H10000].End(xlUp).Offset(1, 0)   'Local Amount
        
        On Error Resume Next
        ActiveSheet.ShowAllData
        On Error GoTo 0
        
        wsOutput.Select
        
        wsOutput.Range([A10000].End(xlUp).Offset(1, 0), [B2].End(xlDown).Offset(0, -1)).value = "ZFI016"
    
    End If

sExecutionExitMode = "Traditional"
Call PrintOutFunction_Lib(sExecutionID, sUser, sExecutionExitMode, sFunctionName, sFunctionParameters)

End Sub

Sub LoadFBL1NRecords(sExecutionID)

Dim rn                              As Range
Dim wsOutput                        As Worksheet
Dim sFunctionName                   As String
Dim sFunctionParameters             As String
Dim sExecutionExitMode              As String

sFunctionName = "Sub LoadFBL1NRecords(sExecutionID)"
sFunctionParameters = sExecutionID
Call PrintInFunction_Lib(sExecutionID, sUser, sFunctionName, sFunctionParameters)
    
    If boltemVcFBL1N = True Then
    
        wsFBL1N.Select
        Set rn = wsFBL1N.[A1].CurrentRegion
        Set wsOutput = Sheets("3.1 - Conciliação VC")
        On Error Resume Next
        ActiveSheet.ShowAllData
        On Error GoTo 0
       
        rn.AutoFilter Field:=23, Criteria1:=">0", Operator:=xlOr, Criteria2:="<0"
       
        'Range([E2], [E50000].End(xlUp)).SpecialCells(xlCellTypeVisible).Copy Destination:=Sheets("3.1 - Conciliação VC").[B10000].End(xlUp).Offset(1, 0) 'Segment
         rn.Range([H2], [H50000].End(xlUp)).SpecialCells(xlCellTypeVisible).Copy Destination:=wsOutput.[B100000].End(xlUp).Offset(1, 0) 'Segment
         Range([F2], [F50000].End(xlUp)).SpecialCells(xlCellTypeVisible).Copy Destination:=wsOutput.[C100000].End(xlUp).Offset(1, 0) 'Plant
        'Range([R2], [S50000].End(xlUp)).SpecialCells(xlCellTypeVisible).Copy Destination:=Sheets("3.1 - Conciliação VC").[E10000].End(xlUp).Offset(1, 0) 'Account / SAP Account Name
         rn.Range([I2], [J50000].End(xlUp)).SpecialCells(xlCellTypeVisible).Copy Destination:=wsOutput.[E100000].End(xlUp).Offset(1, 0)  'Account / SAP Account Name
        'Range([A2], [A50000].End(xlUp)).SpecialCells(xlCellTypeVisible).Copy Destination:=Sheets("3.1 - Conciliação VC").[D10000].End(xlUp).Offset(1, 0) 'Document Number
         rn.Range([B2], [B50000].End(xlUp)).SpecialCells(xlCellTypeVisible).Copy Destination:=wsOutput.[D100000].End(xlUp).Offset(1, 0) 'Document Number
        'Range([U2], [U50000].End(xlUp)).SpecialCells(xlCellTypeVisible).Copy Destination:=Sheets("3.1 - Conciliação VC").[H10000].End(xlUp).Offset(1, 0) 'Exchange Rate Variation
         rn.Range([W2], [W50000].End(xlUp)).SpecialCells(xlCellTypeVisible).Copy Destination:=wsOutput.[H100000].End(xlUp).Offset(1, 0) 'Exchange Rate Variation
        'Range([M2], [M50000].End(xlUp)).SpecialCells(xlCellTypeVisible).Copy Destination:=Sheets("3.1 - Conciliação VC").[G10000].End(xlUp).Offset(1, 0) 'Local Currency
         rn.Range([P2], [P50000].End(xlUp)).SpecialCells(xlCellTypeVisible).Copy Destination:=wsOutput.[G100000].End(xlUp).Offset(1, 0) 'Local Currency
         
        On Error Resume Next
        ActiveSheet.ShowAllData
        On Error GoTo 0
         
         wsOutput.Select
         wsOutput.Range([A10000].End(xlUp).Offset(1, 0), [B2].End(xlDown).Offset(0, -1)).value = "FBL1N"
    
    End If

sExecutionExitMode = "Traditional"
Call PrintOutFunction_Lib(sExecutionID, sUser, sExecutionExitMode, sFunctionName, sFunctionParameters)

End Sub

Sub LoadReavaliacaoCambialRecords(sExecutionID)

Dim rn                              As Range
Dim wsOutput                        As Worksheet
Dim sFunctionName                   As String
Dim sFunctionParameters             As String
Dim sExecutionExitMode              As String

sFunctionName = "Sub LoadReavaliacaoCambialRecords(sExecutionID)"
sFunctionParameters = sExecutionID
Call PrintInFunction_Lib(sExecutionID, sUser, sFunctionName, sFunctionParameters)

    If boltemVcReavaliacaoCambial = True Then
    
        wsFAGLFCV.Select
        
        Set rn = wsFAGLFCV.[A1].CurrentRegion
        Set wsOutput = Sheets("3.1 - Conciliação VC")
        On Error Resume Next
        ActiveSheet.ShowAllData
        On Error GoTo 0
    
        Range([F2], [G5000].End(xlUp)).SpecialCells(xlCellTypeVisible).Copy Destination:=Sheets("3.1 - Conciliação VC").[B10000].End(xlUp).Offset(1, 0) 'Segment
        Range([B2], [C5000].End(xlUp)).SpecialCells(xlCellTypeVisible).Copy Destination:=Sheets("3.1 - Conciliação VC").[E10000].End(xlUp).Offset(1, 0) 'Acount / Acount Name
        Range([E2], [E5000].End(xlUp)).SpecialCells(xlCellTypeVisible).Copy Destination:=Sheets("3.1 - Conciliação VC").[D10000].End(xlUp).Offset(1, 0) 'Document Number
        Range([L2], [L5000].End(xlUp)).SpecialCells(xlCellTypeVisible).Copy Destination:=Sheets("3.1 - Conciliação VC").[G10000].End(xlUp).Offset(1, 0) 'Document Number
        Range([Q2], [Q5000].End(xlUp)).SpecialCells(xlCellTypeVisible).Copy Destination:=Sheets("3.1 - Conciliação VC").[H10000].End(xlUp).Offset(1, 0) 'Exchange Rate Variation
         
        On Error Resume Next
        ActiveSheet.ShowAllData
        On Error GoTo 0
    
        wsOutput.Select
        wsOutput.Range([A10000].End(xlUp).Offset(1, 0), [B2].End(xlDown).Offset(0, -1)).value = "Reavaliação Cambial"
        
    End If

sExecutionExitMode = "Traditional"
Call PrintOutFunction_Lib(sExecutionID, sUser, sExecutionExitMode, sFunctionName, sFunctionParameters)

End Sub
