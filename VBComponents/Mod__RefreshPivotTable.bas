Attribute VB_Name = "Mod__RefreshPivotTable"
Option Explicit

Sub RefreshDataMatching(sExecutionID)

Dim sFunctionName                   As String
Dim sFunctionParameters             As String
Dim sExecutionExitMode              As String

sFunctionName = "Sub RefreshDataMatching(sExecutionID)"
sFunctionParameters = sExecutionID
Call PrintInFunction_Lib(sExecutionID, sUser, sFunctionName, sFunctionParameters)
    
    Call RefreshPT_AtivoePassivo(sExecutionID)
    Call RefreshPT_VC(sExecutionID)
    Call RefreshDifferences(sExecutionID)

sExecutionExitMode = "Traditional"
Call PrintOutFunction_Lib(sExecutionID, sUser, sExecutionExitMode, sFunctionName, sFunctionParameters)


End Sub

Sub RefreshPT_AtivoePassivo(sExecutionID)

Dim sFunctionName                   As String
Dim sFunctionParameters             As String
Dim sExecutionExitMode              As String

sFunctionName = "Sub RefreshPT_AtivoePassivo(sExecutionID)"
sFunctionParameters = sExecutionID
Call PrintInFunction_Lib(sExecutionID, sUser, sFunctionName, sFunctionParameters)



    For Each ws In Worksheets
    
    If ws.name = "1.2 - PT_Ativo" Or _
       ws.name = "2.2 - PT_Passivo" Or _
       ws.name = "Resumo Aging BRA" Or _
       ws.name = "Resumo Aging ARG" Then
          
            ws.Select
            
            If ws.name = "1.2 - PT_Ativo" Or ws.name = "2.2 - PT_Passivo" Then
                'Debug.Print ws.Name
                Set rnRange = [D1:E30,I1:J30000]
                Call ClearContents_Lib(sExecutionID, sUser, rnRange)   'DeleteDifferences
            End If
            
            For Each ptPivotTable In ActiveSheet.PivotTables
                           
                'Concilia��o Ativo
                 If Left(ptPivotTable.SourceData, 25) = "'1.1 - Concilia��o Ativo'" Then
                    ptPivotTable.ChangePivotCache ActiveWorkbook.PivotCaches.Create(SourceType:=xlDatabase, SourceData:= _
                    "1.1 - Concilia��o Ativo!R1C1:R" & Sheets("1.1 - Concilia��o Ativo").[A1].End(xlDown).Row & "C8", Version:=6)
                
                 'Concilia��o Passivo
                 ElseIf Left(ptPivotTable.SourceData, 27) = "'2.1 - Concilia��o Passivo'" Then
                    ptPivotTable.ChangePivotCache ActiveWorkbook.PivotCaches.Create(SourceType:=xlDatabase, SourceData:= _
                    "2.1 - Concilia��o Passivo!R1C1:R" & Sheets("2.1 - Concilia��o Passivo").[A1].End(xlDown).Row & "C8", Version:=6)
    
                'Resumo Aging BRA e Resumo Aging ARG
                 ElseIf Left(ptPivotTable.SourceData, 6) = "ZFI016" Then
                    ptPivotTable.ChangePivotCache ActiveWorkbook.PivotCaches.Create(SourceType:=xlDatabase, SourceData:= _
                    "ZFI016!R1C1:R" & Sheets("ZFI016").[A1].End(xlDown).Row & "C24", Version:=6)
                 End If
                 
                 'Retirar Filtros Ativos
                 On Error Resume Next
                    ptPivotTable.PivotFields("Segment").ClearAllFilters
                    ptPivotTable.PivotFields("Document Number").ClearAllFilters
                    ptPivotTable.PivotFields("Remarks").ClearAllFilters
                 On Error GoTo 0
                 
                 ptPivotTable.PivotCache.Refresh
                 Cells.EntireColumn.AutoFit
                 
            Next ptPivotTable
            
    End If
    
    Next ws
    
sExecutionExitMode = "Traditional"
Call PrintOutFunction_Lib(sExecutionID, sUser, sExecutionExitMode, sFunctionName, sFunctionParameters)
    
    
End Sub

Sub RefreshPT_VC(sExecutionID)

Dim sFunctionName                   As String
Dim sFunctionParameters             As String
Dim sExecutionExitMode              As String

sFunctionName = "Sub RefreshPT_VC(sExecutionID)"
sFunctionParameters = sExecutionID
    Call PrintInFunction_Lib(sExecutionID, sUser, sFunctionName, sFunctionParameters)
    
    Sheets("3.2 - PT_Varia��o Cambial").Select
    
    Set rnRange = [A1:O1000]
    
    Call ClearContents_Lib(sExecutionID, sUser, rnRange)   'DeleteDifferences
    
    If boltemVcReavaliacaoCambial = True Or boltemVcZFI016 = True Or boltemVcFBL1N = True Then
    
        For Each ptPivotTable In ActiveSheet.PivotTables
            
            ptPivotTable.ChangePivotCache ActiveWorkbook.PivotCaches.Create(SourceType:=xlDatabase, SourceData:= _
            "3.1 - Concilia��o VC!R1C1:R" & Sheets("3.1 - Concilia��o VC").[A1].End(xlDown).Row & "C8", Version:=6)
            
        ptPivotTable.PivotCache.Refresh
        Cells.EntireColumn.AutoFit
        
        Next ptPivotTable
        
        'Retirar Filtros Ativos
        On Error Resume Next
           ptPivotTable.PivotFields("Segment").ClearAllFilters
           ptPivotTable.PivotFields("Document Number").ClearAllFilters
           ptPivotTable.PivotFields("Remarks").ClearAllFilters
        On Error GoTo 0
        
    End If
    
sExecutionExitMode = "Traditional"
Call PrintOutFunction_Lib(sExecutionID, sUser, sExecutionExitMode, sFunctionName, sFunctionParameters)
        
End Sub

Sub RefreshDifferences(sExecutionID)

Dim sFunctionName                   As String
Dim sFunctionParameters             As String
Dim sExecutionExitMode              As String

sFunctionName = "Sub RefreshDifferences(sExecutionID)"
sFunctionParameters = sExecutionID
Call PrintInFunction_Lib(sExecutionID, sUser, sFunctionName, sFunctionParameters)

    Call RefreshAssetsDifferences(sExecutionID)
    Call RefreshLiabilitiesDifferences(sExecutionID)
    
    If boltemVcReavaliacaoCambial = True Or boltemVcZFI016 = True Or boltemVcFBL1N = True Then
        Call RefreshVcDifferences(sExecutionID)
    End If
    
sExecutionExitMode = "Traditional"
Call PrintOutFunction_Lib(sExecutionID, sUser, sExecutionExitMode, sFunctionName, sFunctionParameters)

End Sub


Sub RefreshAssetsDifferences(sExecutionID)

Dim sFunctionName                   As String
Dim sFunctionParameters             As String
Dim sExecutionExitMode              As String

sFunctionName = "Sub RefreshAssetsDifferences(sExecutionID)"
sFunctionParameters = sExecutionID
Call PrintInFunction_Lib(sExecutionID, sUser, sFunctionName, sFunctionParameters)
    
    Sheets("1.2 - PT_Ativo").Select
    [B3:D100,G3:I30000].Style = "Comma"
        
    i = 3
    Do While Cells(i, 1).value <> ""
    If Cells(i + 1, 1).value <> "" Then
        Cells(i, 4).value = Cells(i, 3).value - Cells(i, 2).value
        Cells(i, 4).Font.ColorIndex = 3
    End If
    i = i + 1
    Loop
        
    i = 5
    Do While Cells(i, 6).value <> ""
        Cells(i, 9).value = Cells(i, 8).value - Cells(i, 7).value
        Cells(i, 9).Font.ColorIndex = 3
    i = i + 1
    Loop
    
    Cells.EntireColumn.AutoFit
    
sExecutionExitMode = "Traditional"
Call PrintOutFunction_Lib(sExecutionID, sUser, sExecutionExitMode, sFunctionName, sFunctionParameters)

End Sub


Sub RefreshLiabilitiesDifferences(sExecutionID)

Dim sFunctionName                   As String
Dim sFunctionParameters             As String
Dim sExecutionExitMode              As String

sFunctionName = "Sub RefreshLiabilitiesDifferences(sExecutionID)"
sFunctionParameters = sExecutionID
Call PrintInFunction_Lib(sExecutionID, sUser, sFunctionName, sFunctionParameters)
    
    Sheets("2.2 - PT_Passivo").Select
    [B3:D500,G3:I500].Style = "Comma"
        
    i = 3
    Do While Cells(i, 1).value <> ""
    If Cells(i + 1, 1).value <> "" Then
        Cells(i, 4).value = Cells(i, 3).value - Cells(i, 2).value
        Cells(i, 4).Font.ColorIndex = 3
    End If
    i = i + 1
    Loop
        
    i = 5
    Do While Cells(i, 6).value <> ""
        Cells(i, 9).value = Cells(i, 8).value - Cells(i, 7).value
        Cells(i, 9).Font.ColorIndex = 3
    i = i + 1
    Loop
       
    Cells.EntireColumn.AutoFit

sExecutionExitMode = "Traditional"
Call PrintOutFunction_Lib(sExecutionID, sUser, sExecutionExitMode, sFunctionName, sFunctionParameters)

End Sub

Sub RefreshVcDifferences(sExecutionID)

Dim sFunctionName                   As String
Dim sFunctionParameters             As String
Dim sExecutionExitMode              As String

sFunctionName = "Sub RefreshVcDifferences(sExecutionID)"
sFunctionParameters = sExecutionID
Call PrintInFunction_Lib(sExecutionID, sUser, sFunctionName, sFunctionParameters)
    
    'Conferir C�digo
    'No per�odo jan/2021 n�o teve varia��o cambial para AR portanto deve-se testar a rotina abaixo num
    'per�odo que tiver registros
    
    Sheets("3.2 - PT_Varia��o Cambial").Select
    
    If boltemVcZFI016 = True And boltemVcFBL1N = True And boltemVcReavaliacaoCambial = True Then
    
        [B3:E1000].Style = "comma"
        [I5:N1000].Style = "comma"
        
        i = 3
        Do While Cells(i, 1).value <> ""
        If Cells(i, 1).value <> "" Then
            Cells(i, 5).value = Cells(i, 2).value - Cells(i, 3).value - Cells(i, 4).value
            Cells(i, 5).Font.ColorIndex = 3
        End If
        i = i + 1
        Loop
            
        i = 3
        Do While Cells(i, 9).value <> ""
            Cells(i, 13).value = Cells(i, 12).value - Cells(i, 11).value - Cells(i, 10).value
            Cells(i, 13).Font.ColorIndex = 3
        i = i + 1
        Loop
        
    ElseIf boltemVcZFI016 = False And boltemVcFBL1N = True And boltemVcReavaliacaoCambial = True Or _
           boltemVcZFI016 = True And boltemVcFBL1N = False And boltemVcReavaliacaoCambial = True Or _
           boltemVcZFI016 = True And boltemVcFBL1N = True And boltemVcReavaliacaoCambial = False Then
    
        [B3:D100].Style = "comma"
        [I5:K1000].Style = "comma"
        
        i = 3
        Do While Cells(i, 1).value <> ""
        If Cells(i, 1).value <> "" Then
            Cells(i, 4).value = Cells(i, 3).value - Cells(i, 2).value
            Cells(i, 4).Font.ColorIndex = 3
        End If
        i = i + 1
        Loop
            
        i = 3
        Do While Cells(i, 9).value <> ""
            Cells(i, 12).value = Cells(i, 11).value - Cells(i, 10).value
            Cells(i, 12).Font.ColorIndex = 3
        i = i + 1
        Loop
    
    End If
    
    
    With Cells
        .Font.name = "Calibri Light"
        .Font.Size = 10
        .VerticalAlignment = xlCenter
    End With
    
    Rows.EntireRow.RowHeight = 15
        
    Cells.EntireColumn.AutoFit

sExecutionExitMode = "Traditional"
Call PrintOutFunction_Lib(sExecutionID, sUser, sExecutionExitMode, sFunctionName, sFunctionParameters)

End Sub






