Attribute VB_Name = "Mod__ProcessSuportFiles"
Option Explicit

Sub RunAllSuportFileScript(sExecutionID, sUser)

Dim sOriginalFilesLocation              As String
Dim sSuportFilesLocation                As String
Dim sFunctionName                       As String
Dim sFunctionParameters                 As String
Dim sExecutionExitMode                  As String


sFunctionName = "Sub RunAllSuportFileScript(sExecutionID, sUser)"
sFunctionParameters = sExecutionID & ", " & sUser
Call PrintInFunction_Lib(sExecutionID, sUser, sFunctionName, sFunctionParameters)
    

    'FAGLL03
    ChDir sAutomationsRootPath
    Workbooks.Open Filename:=sAutomationsRootPath & "fagll03_vendor\FAGLL03.xlsm", UpdateLinks:=0
    Application.Run "'FAGLL03.xlsm'!Main"
    Windows("FAGLL03.xlsm").Activate
    ActiveWorkbook.Save
    ActiveWindow.Close

    'FBL1N2.xlsm
    ChDir sAutomationsRootPath
    Workbooks.Open Filename:=sAutomationsRootPath & "fbl1n2\FBL1N2.xlsm", UpdateLinks:=0
    Application.Run "'FBL1N2.xlsm'!Main"
    Windows("FBL1N2.xlsm").Activate
    ActiveWorkbook.Save
    ActiveWindow.Close

    'ZFI016.xlsm
    ChDir sAutomationsRootPath
    Workbooks.Open Filename:=sAutomationsRootPath & "zfi016_vendor\ZFI016.xlsm", UpdateLinks:=0
    Application.Run "'ZFI016.xlsm'!Main"
    Windows("ZFI016.xlsm").Activate
    ActiveWorkbook.Save
    ActiveWindow.Close

    'FAGLFCV.xlsm
    ChDir sAutomationsRootPath
    Workbooks.Open Filename:=sAutomationsRootPath & "faglfcv_vendor\FAGLFCV.xlsm", UpdateLinks:=0
    Application.Run "'FAGLFCV.xlsm'!Main"
    Windows("FAGLFCV.xlsm").Activate
    ActiveWorkbook.Save
    ActiveWindow.Close

    'FBL1N.xlsm
    ChDir sAutomationsRootPath
    Workbooks.Open Filename:=sAutomationsRootPath & "fbl1n\FBL1N.xlsm", UpdateLinks:=0
    Application.Run "'FBL1N.xlsm'!Main"
    Windows("FBL1N.xlsm").Activate
    ActiveWorkbook.Save
    ActiveWindow.Close
    
    sAutomationID = SetAutomationID
    
sExecutionExitMode = "Traditional"
Call PrintOutFunction_Lib(sExecutionID, sUser, sExecutionExitMode, sFunctionName, sFunctionParameters)

End Sub

