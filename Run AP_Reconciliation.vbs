Dim xlApp, xlBook 
Dim ProgramPath, WshShell, ProgramArgs, WaitOnReturn, intWindowStyle, Scriptdi, Filename

Scriptdir = CreateObject("Scripting.FileSystemObject").GetParentFolderName(WScript.ScriptFullName)

'------------------------------------------------------------------------------------------------------------------------------
'Run Automation Main Code
'------------------------------------------------------------------------------------------------------------------------------
Set xlApp = CreateObject("Excel.Application") 
Filename = "AP_Reconciliation.xlsm"
Set xlBook = xlApp.Workbooks.Open(Scriptdir & "\" & Filename) 
xlApp.Run Filename & "!Mod___Main.Main"
'xlBook.Save 
xlBook.Close 
xlApp.Quit 

WScript.Quit